#include "Zangeneh.h"
#include "Bibikov.h"
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

string Menu[MENU_ELEMENTS_AMOUNT] = { "1. Initialize matrix", "2. Print matrix", "3. All maximum elements of even colums of matrix", "4. Side and main diagonal comparison", "5. The smallest among numbers below the main diagonal of the matrix" };
extern int Matrix[MATRIX_SIZE][MATRIX_SIZE];
extern bool Initialized;

void SideMainDiagonalCompare()
{
	system("cls");

	if (!Initialized)
	{
		cout << "Error, matrix is NOT initialized!" << endl;
		cout << "Please, initialize matrix first" << endl;
		cout << "Press Enter to return to menu" << endl;
		char key = '\0';
		while (key != '\n') {
			key = getchar();
		}
		return;
	}

	bool equalFlag = 1;
	for (int counter1 = 0; counter1 < MATRIX_SIZE; counter1++) {
		for (int counter2 = (MATRIX_SIZE - 1); counter2 >= 0; counter2--) {
			if (Matrix[counter1][counter1] != Matrix[counter2][counter1])
			{
				equalFlag = 0;
			}
		}
	}

	if (equalFlag)
	{
		cout << "The side diagonal is elementwise equal to the main" << endl;
	}
	else
	{
		cout << "The side diagonal is NOT elementwise equal to the main" << endl;
	}

	cout << "Press Enter to return to menu" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void SearchMinElementBelowMainDiagonal()
{
	system("cls");

	if (!Initialized)
	{
		cout << "Error, matrix is NOT initialized!" << endl;
		cout << "Please, initialize matrix first" << endl;
		cout << "Press Enter to return to menu" << endl;
		char key = '\0';
		while (key != '\n') {
			key = getchar();
		}
		return;
	}

	int min = Matrix[1][1];
	for (int counter1 = 0; counter1 < MATRIX_SIZE; counter1++) {
		for (int counter2 = 0; counter2 < MATRIX_SIZE; counter2++) {
			if (counter2 <= counter1)
			{
				if (Matrix[counter1][counter2] < min)
				{
					min = Matrix[counter1][counter2];
				}
			}
		}
	}
	cout << "The minimum element below the main diagonal of the matrix is: " << min << endl;

	cout << "Press Enter to return to menu" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void PrintMenu()
{
	system("cls");
	cout << "Menu:" << endl;
	for (int counter = 0; counter < MENU_ELEMENTS_AMOUNT; counter++) {
		cout << Menu[counter] << endl;
	}
}