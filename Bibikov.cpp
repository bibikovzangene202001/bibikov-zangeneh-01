#include "Bibikov.h"
#include "Zangeneh.h"
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

int Matrix[MATRIX_SIZE][MATRIX_SIZE] = { 0 };
bool Initialized = 0;

void MatrixInitialization()
{
	system("cls");
	cout << "Matrix initialization:" << endl;
	for (int counter1 = 0; counter1 < MATRIX_SIZE; counter1++) {
		for (int counter2 = 0; counter2 < MATRIX_SIZE; counter2++) {
			Matrix[counter1][counter2] = (rand() % 21) - 10;
		}
	}

	Initialized = 1;
	cout << "The matrix for 17 elements has been initialized" << endl;

	cout << "Press Enter to return to menu" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void PrintMatrix()
{
	system("cls");

	if (!Initialized)
	{
		cout << "Error, matrix is NOT initialized!" << endl;
		cout << "Please, initialize matrix first" << endl;
		cout << "Press Enter to return to menu" << endl;
		char key = '\0';
		while (key != '\n') {
			key = getchar();
		}
		return;
	}

	cout << "Matrix:" << endl;
	for (int counter1 = 0; counter1 < MATRIX_SIZE; counter1++) {
		for (int counter2 = 0; counter2 < MATRIX_SIZE; counter2++) {
			cout << setw(5) << Matrix[counter1][counter2];
		}
		cout << endl << endl;
	}

	cout << "Press Enter to return to menu" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void MaxEvenColumnSearch()
{
	system("cls");

	if (!Initialized)
	{
		cout << "Error, matrix is NOT initialized!" << endl;
		cout << "Please, initialize matrix first" << endl;
		cout << "Press Enter to return to menu" << endl;
		char key = '\0';
		while (key != '\n') {
			key = getchar();
		}
		return;
	}

	int MaxArray[MATRIX_SIZE] = { 0 };
	for (int columnCounter = 1; columnCounter < MATRIX_SIZE; columnCounter += 2) {
		MaxArray[columnCounter] = Matrix[0][columnCounter];
		for (int lineCounter = 0; lineCounter < MATRIX_SIZE; lineCounter++) {
			if (Matrix[lineCounter][columnCounter] > MaxArray[columnCounter])
			{
				MaxArray[columnCounter] = Matrix[lineCounter][columnCounter];
			}
		}
	}

	cout << "The maximum elements are:" << endl;
	for (int counter = 1; counter < MATRIX_SIZE; counter += 2) {
		cout << "Column: " << counter << " The maximum element is: " << MaxArray[counter] << endl;
	}

	cout << "Press Enter to return to menu" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}