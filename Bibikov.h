#pragma once

const int MATRIX_SIZE = 17;
const int MENU_ELEMENTS_AMOUNT = 5;

void MatrixInitialization();
void PrintMatrix();
void MaxEvenColumnSearch();