#include <iostream>
#include "Bibikov.h"
#include "Zangeneh.h"

using namespace std;

void main()
{
	int select = 0;
	do {
		select = 0;
		system("cls");
		PrintMenu();
		cout << "Enter the number of option you want to choose, or enter 0 to end the program" << endl;
		cout << "Option number: ";
		cin >> select;
		cin.clear();
		while (cin.get() != '\n');
		switch (select)
		{
		case 1: MatrixInitialization(); break;
		case 2: PrintMatrix();; break;
		case 3: MaxEvenColumnSearch(); break;
		case 4: SideMainDiagonalCompare();; break;
		case 5: SearchMinElementBelowMainDiagonal();; break;
		case 0: exit(0);
		}
	} while (select != 0);
}